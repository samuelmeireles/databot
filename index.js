const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch({
  	headless: false,
  	// slowMo: 50
  });
  const page = await browser.newPage();
  await page.goto('http://anvssdf83/datavisa/index.asp');
  
  // Login
  await page.evaluate((campo) => {
  	document.querySelector(campo).value = 'JUSCELINO.MOURA';
  }, 'body > form > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > input[type="text"]');
  await page.click('body > form > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > input[type="password"]');
  await page.keyboard.type('123456');
  await page.click('body > form > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > th > input:nth-child(2)');
  
  // Vai para a página de distribuição
  await page.waitForSelector("#menuh > ul:nth-child(1) > li > a");
  await page.goto("http://anvssdf83/datavisa/paf/distribuicao.asp");

  // Filtra o assunto e seleciona todos do assunto
  await page.select('body > table > tbody > tr:nth-child(2) > td > div:nth-child(1) > select', '9611');
  await page.click('#tb_pendentes > thead > tr > th:nth-child(1) > input[type="checkbox"]');
  await page.click('body > table > tbody > tr:nth-child(2) > td > div:nth-child(9) > input[type="radio"]:nth-child(3)');
  
  // Aloca para o posto e finaliza
  await page.select('#cmb_posto', "290");
  // await page.click('#btnAvancar');
  
  // await browser.close();
})();